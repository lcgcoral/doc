<?php
/*
  Ross Scrivener http://scrivna.com
  PHP file diff implementation

  Much credit goes to...
  Paul's Simple Diff Algorithm v 0.1
  (C) Paul Butler 2007 <http://www.paulbutler.org/>
  May be used and distributed under the zlib/libpng license.

  ... for the actual diff code, i changed a few things and implemented a pretty interface to it.
*/
class diff
{

  var $changes = array();
  var $diff = array();
  var $linepadding = null;

  function doDiff($old, $new)
  {
    if (!is_array($old)) $old = file($old);
    if (!is_array($new)) $new = file($new);
    foreach($old as $oindex => $ovalue)
    {
      $nkeys = array_keys($new, $ovalue);
      foreach($nkeys as $nindex){
        $matrix[$oindex][$nindex] = isset($matrix[$oindex - 1][$nindex - 1]) ? $matrix[$oindex - 1][$nindex - 1] + 1 : 1;
        if($matrix[$oindex][$nindex] > $maxlen)
        {
          $maxlen = $matrix[$oindex][$nindex];
          $omax = $oindex + 1 - $maxlen;
          $nmax = $nindex + 1 - $maxlen;
        }
      }
    }
    if($maxlen == 0) return array(array('d'=>$old, 'i'=>$new));
    return array_merge( $this->doDiff(array_slice($old, 0, $omax),
                                      array_slice($new, 0, $nmax)),
                        array_slice($new, $nmax, $maxlen),
                        $this->doDiff(array_slice($old, $omax + $maxlen),
                                      array_slice($new, $nmax + $maxlen)));

  }

  function diffWrap($old, $new)
  {
    $this->diff = $this->doDiff($old, $new);
    $this->changes = array();
    $ndiff = array();
    foreach ($this->diff as $line => $k)
    {
      if(is_array($k))
      {
        if (isset($k['d'][0]) || isset($k['i'][0]))
        {
          $this->changes[] = $line;
          $ndiff[$line] = $k;
        }
      }
      else
      {
        $ndiff[$line] = $k;
      }
    }
    $this->diff = $ndiff;
    return $this->diff;
  }

  function formatcode($code)
  {
    $code = htmlentities($code);
    $code = str_replace(" ",'&nbsp;',$code);
    $code = str_replace("\t",'&nbsp;&nbsp;&nbsp;&nbsp;',$code);
    return $code;
  }

  function showline($line)
  {
    if ($this->linepadding === 0)
    {
      if (in_array($line,$this->changes)) return true;
      return false;
    }
    if(is_null($this->linepadding)) return true;
    $start = (($line - $this->linepadding) > 0) ? ($line - $this->linepadding) : 0;
    $end = ($line + $this->linepadding);
    //echo '<br />'.$line.': '.$start.': '.$end;
    $search = range($start,$end);
    //pr($search);
    foreach($search as $k)
    {
      if (in_array($k,$this->changes)) return true;
    }
    return false;
  }

  function inline($old, $new, $linepadding=null)
  {
    $this->linepadding = $linepadding;
    $ret = '<pre><table width="100%" border="0" cellspacing="0" cellpadding="0" class="code">';
    $ret.= '<tr><td>Old</td><td>New</td><td></td></tr>';
    $count_old = 1;
    $count_new = 1;
    $insert = false;
    $delete = false;
    $truncate = false;
    $diff = $this->diffWrap($old, $new);
    foreach($diff as $line => $k)
    {
      if ($this->showline($line))
      {
        $truncate = false;
        if(is_array($k))
        {
          foreach ($k['d'] as $val)
          {
            $class = '';
            if (!$delete)
            {
              $delete = true;
              $class = 'first';
              if ($insert) $class = '';
              $insert = false;
            }
            $ret.= '<tr><th>'.$count_old.'</th>';
            $ret.= '<th>&nbsp;</th>';
            $ret.= '<td class="del '.$class.'">'.$this->formatcode($val).'</td>';
            $ret.= '</tr>';
            $count_old++;
          }
          foreach ($k['i'] as $val)
          {
            $class = '';
            if (!$insert)
            {
              $insert = true;
              $class = 'first';
              if ($delete) $class = '';
              $delete = false;
            }
            $ret.= '<tr><th>&nbsp;</th>';
            $ret.= '<th>'.$count_new.'</th>';
            $ret.= '<td class="ins '.$class.'">'.$this->formatcode($val).'</td>';
            $ret.= '</tr>';
            $count_new++;
          }
        }
        else
        {
          $class = ($delete) ? 'del_end' : '';
          $class = ($insert) ? 'ins_end' : $class;
          $delete = false;
          $insert = false;
          $ret.= '<tr><th>'.$count_old.'</th>';
          $ret.= '<th>'.$count_new.'</th>';
          $ret.= '<td class="'.$class.'">'.$this->formatcode($k).'</td>';
          $ret.= '</tr>';
          $count_old++;
          $count_new++;
        }
      }
      else
      {
        $class = ($delete) ? 'del_end' : '';
        $class = ($insert) ? 'ins_end' : $class;
        $delete = false;
        $insert = false;
        if (!$truncate)
        {
          $truncate = true;
          $ret.= '<tr><th>...</th>';
          $ret.= '<th>...</th>';
          $ret.= '<td class="truncated '.$class.'">&nbsp;</td>';
          $ret.= '</tr>';
        }
        $count_old++;
        $count_new++;
      }
    }
    $ret.= '</table></pre>';
    return $ret;
  }
}
?>

<style type="text/css">
table.code {
	border: 1px solid #ddd;
	border-spacing: 0;
	border-top: 0;
	empty-cells: show;
	font-size: 12px;
	line-height: 110%;
	padding: 0;
	margin: 0;
	width: 100%;
}

table.code th {
	background: #eed;
	color: #886;
	font-weight: normal;
	padding: 0 .5em;
	text-align: right;
	border-right: 1px solid #d7d7d7;
	border-top: 1px solid #998;
	font-size: 11px;
	width: 35px;
}

table.code td {
	background: #fff;
	font: normal 11px monospace;
	overflow: auto;
	padding: 1px 2px;
}

table.code td.del {
	background-color: #FDD;
	border-left: 1px solid #C00;
	border-right: 1px solid #C00;
}
table.code td.del.first {
	border-top: 1px solid #C00;
}
table.code td.ins {
	background-color: #DFD;
	border-left: 1px solid #0A0;
	border-right: 1px solid #0A0;
}
table.code td.ins.first {
	border-top: 1px solid #0A0;
}
table.code td.del_end {
	border-top: 1px solid #C00;
}
table.code td.ins_end {
	border-top: 1px solid #0A0;
}
table.code td.truncated {
	background-color: #f7f7f7;
}
</style>

<?php
$vold=$_GET["old"];
$vnew=$_GET["new"];
//echo $vold . '<br>';
//echo $vnew . '<br>';
$voldsuf1=substr($vold,-1);
$vnewsuf1=substr($vnew,-1);
//echo $voldsuf1 . '<br>';
//echo $vnewsuf1 . '<br>';
$voldsuf5=substr($vold,-5);
$vnewsuf5=substr($vnew,-5);
//echo $voldsuf5 . '<br>';
//echo $vnewsuf5 . '<br>';
if( $voldsuf1 === 'a' || $voldsuf1 === 'b' || $voldsuf1 === 'c' || $voldsuf1 === 'd' || $voldsuf1 === 'e' || $voldsuf1 === 'f' || $voldsuf1 === 'g' || $voldsuf1 === 'h' || $voldsuf1 === 'i' || $voldsuf1 === 'j' )
{
  $voldint=substr($vold,0,-1);
}
elseif( $voldsuf5 === 'swan1' || $voldsuf5 === 'swan2' || $voldsuf5 === 'swan3' || $voldsuf5 === 'swan4' || $voldsuf5 === 'swan5' || $voldsuf5 === 'swan6' || $voldsuf5 === 'swan7' || $voldsuf5 === 'swan8' || $voldsuf5 === 'swan9' )
{
  $voldint=substr($vold,0,-5);
}
else
{
  $voldint=$vold; 
}
if( $vnewsuf1 === 'a' || $vnewsuf1 === 'b' || $vnewsuf1 === 'c' || $vnewsuf1 === 'd' || $vnewsuf1 === 'e' || $vnewsuf1 === 'f' || $vnewsuf1 === 'g' || $vnewsuf1 === 'h' || $vnewsuf1 === 'i' || $vnewsuf1 === 'j' )
{
  $vnewint=substr($vnew,0,-1);
}
elseif( $vnewsuf5 === 'swan1' || $vnewsuf5 === 'swan2' || $vnewsuf5 === 'swan3' || $vnewsuf5 === 'swan4' || $vnewsuf5 === 'swan5' || $vnewsuf5 === 'swan6' || $vnewsuf5 === 'swan7' || $vnewsuf5 === 'swan8' || $vnewsuf5 === 'swan9' )
{
  $vnewint=substr($vnew,0,-5);
}
else
{
  $vnewint=$vnew;
}
//echo $voldint . '<br>';
//echo $vnewint . '<br>';
if ( !filter_var($voldint, FILTER_VALIDATE_INT) === false &&
     !filter_var($vnewint, FILTER_VALIDATE_INT) === false )
{
  $fold='https://gitlab.cern.ch/sft/lcgcmake/raw/LCG_'.$vold.'/cmake/toolchain/heptools-'.$vold.'.cmake';
  $fnew='https://gitlab.cern.ch/sft/lcgcmake/raw/LCG_'.$vnew.'/cmake/toolchain/heptools-'.$vnew.'.cmake';
  echo 'Comparing:<br>';
  echo '- '.$fold.'<br>';
  echo '- '.$fnew.'<br>';
  echo '<br>';
  $diff = new diff;
  $text = $diff->inline($fold,$fnew,2);
  echo count($diff->changes).' changes';
  echo $text;
}
else
{
  echo 'Invalid inputs: \'old\' and \'new\' must both be integers (optionally with a trailing character between \'a\' and \'j\' or with a trailing \'swanN\' suffix with N between 1 and 9)<br>';
  echo $voldint . '<br>';
  echo $vnewint . '<br>';
}
echo '<br>';
echo 'Based on Ross Scrivener\'s <a href=http://rossscrivener.co.uk/blog/php-file-diff>PHP File Diff</a>';
?>
