<html><head>
      <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
   <title>3. CORAL Architecture</title><meta name="generator" content="DocBook XSL Stylesheets V1.51.1"><link rel="home" href="index.html" title="CORAL 1.9.1 User guide"><link rel="up" href="index.html" title="CORAL 1.9.1 User guide"><link rel="previous" href="Scope.html" title="2. Software Project Scope"><link rel="next" href="DevelopingClientComponents.html" title="4. Developing Client Software Components"><link rel="article" href="index.html" title="CORAL 1.9.1 User guide"><link rel="subsection" href="Architecture.html#d0e79" title="3.1. Primary Use Cases and Software Requirements"><link rel="subsection" href="Architecture.html#d0e114" title="3.2. Architectural Choices"><link rel="subsection" href="Architecture.html#d0e148" title="3.3. Static view of the architecture"></head><body bgcolor="white" text="black" link="#0000FF" vlink="#840084" alink="#0000FF"><div class="navheader"><table width="100%" summary="Navigation header"><tr><th colspan="3" align="center">3. CORAL Architecture</th></tr><tr><td width="20%" align="left"><a accesskey="p" href="Scope.html">Prev</a>&nbsp;</td><th width="60%" align="center">&nbsp;</th><td width="20%" align="right">&nbsp;<a accesskey="n" href="DevelopingClientComponents.html">Next</a></td></tr></table><hr></div><div class="section"><div class="titlepage"><div><h2 class="title" style="clear: both"><a name="Architecture"></a>3. CORAL Architecture</h2></div></div><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="d0e79"></a>3.1. Primary Use Cases and Software Requirements</h3></div></div><p>
CORAL has been developed in order to provide technology independent access to relational databases
for the various components of POOL, the Conditions Database project (COOL) and several applications
and components of the software frameworks of the LHC experiments. The access patterns that
are possible through the CORAL API reflect the needs of the applications of the LHC experiments.
</p><p>
In order to achieve the maximum insulation from the technology-specific implementations, we have
chosen to minimize the SQL exposed through the public API of CORAL to the clients.
Given that the CORAL API is a set of C++ interfaces (mainly pure abstract classes), no SQL types are
exposed to the user for the description of tables, views and query result sets. The SQL to C++ type
mapping (and vice versa) is handled by the technology-specific implementation component.
The need for the maximal SQL insulation from the client software can be demonstrated simply showing
the SQL statements for the two mostly used technologies (MySQL and Oracle) in rather simple tasks:
</p><div class="itemizedlist"><ul type="disc"><li>Creation of a table
<div class="itemizedlist"><ul type="round"><li>MySQL SQL syntax:
<pre class="programlisting">
CREATE TABLE T_t ( I BIGINT, X DOUBLE )
</pre></li><li>Oracle SQL syntax:
<pre class="programlisting">
CREATE TABLE "T_t" ( I NUMBER(20), X BINARY_DOUBLE )
</pre></li></ul></div></li><li>Query fetching only the first rows of the result set
<div class="itemizedlist"><ul type="round"><li>MySQL SQL syntax:
<pre class="programlisting">
SELECT X FROM T_t ORDER BY I LIMIT 5
</pre></li><li>Oracle SQL syntax:
<pre class="programlisting">
SELECT * FROM (SELECT X FROM "T_t" ORDER BY I) WHERE ROWNUM &lt; 5
</pre></li></ul></div></li></ul></div><p>
The CORAL approach is to present an identical API for such cases, where from the functionality point of
view are identical (among the technologies), but the necessary SQL statement is significantly different.
</p><p>
The design of CORAL addresses the requirement of being able to use different authentication mechanisms
(user/password pairs, certificates) when connecting to a database schema. Moreover, it takes into account
the fact that databases are addressed logically in a user application, which means that there might
be a level of indirection before connecting to an actual database.</p><p>
Finally, in a distributed environment there is always a need for some client-side monitoring of the
operations with a database. CORAL had to allow the software plugging of adapters which send the
relevant information to existing monitoring frameworks.
</p></div><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="d0e114"></a>3.2. Architectural Choices</h3></div></div><p>CORAL has adopted a component-based architecture for several reasons:
</p><div class="itemizedlist"><ul type="disc"><li>Minimization of the compile-time dependencies of any client software component.</li><li>Implementation of the insulation layer in terms of abstract interfaces.</li><li>Enabling the parallel and independent development of the various CORAL components,
hence more efficient unit and integration testing.</li><li>Allowing for multiple implementations of the same interfaces.</li><li>Allowing for the self-consistent deployment of only a subset of the full CORAL library set.</li></ul></div><p>
</p><p>The realization of the component architecture has been based on the plugin management and the component framework
of the <a href="http://seal.cern.ch" target="_top">SEAL</a> project. This allows the development, testing and deployment of
new implementation components without any single change in existing libraries and the configuration. Moreover, the SEAL
component framework and its context hierarchy in particular, allow the various implementation components to make use of
the functionality of other components exclusively through the relevant abstract interfaces without selecting by value
(i.e. by specifying the corresponding logical name) any particular implementation. Instead, a component performs
a search in its context hierarchy for any implementation of a particular interface.
</p><p>
The requirement of flexible authentication mechanisms in a secure, distributed and grid-controlled environment
had a consequence that a connection to a database is attempted, the authentication credentials should not
be provided directly by the client software. Instead, appropriate authentication modules have to be invoked
to provide the necessary credentials depending on the "database role" of the user and the read/write
accessibility of the particular database. This lead to a design of the CORAL API with decoupled sets of
interfaces for database connection and for the retrieval of the authentication credentials. The decoupled
interface sets were therefore implemented by completely decoupled components.
</p><p>
The same approach has been followed for the functionalities of client-side monitoring and database service
lookup. This lead naturally to an architecture with the minimum number of dependency levels among the software
components.
</p><p>Most implementation components implement a set of the public interfaces. There are also cases where
components implement "developer-level" interfaces, which are simply abstract classes, not useable by the client
software, but only by the various CORAL components. This is the case for example of the components implementing
the interfaces related to the client-side monitoring, where the interfaces are called only by the components
implementing the RDBMS-specific functionality.
This can be shown schematically in the following picture:
</p><div class="mediaobject"><table border="0" summary="manufactured viewport for HTML img" cellspacing="0" cellpadding="0"><tr><td align="center"><img src="pics/architecture.png"></td></tr></table></div><p>
</p><p>It has to be noted that there is also a rather granular class hierarchy of C++ exceptions that are thrown in case
of error conditions. The granularity in the exception hierarchy allows the client code to follow different recovery
procedures depending on the source of the error and the conditions under which it occurs.</p></div><div class="sect2"><div class="titlepage"><div><h3 class="title"><a name="d0e148"></a>3.3. Static view of the architecture</h3></div></div><p>
The CORAL packages and their dependencies are shown in the following picture:
</p><div class="mediaobject"><table border="0" summary="manufactured viewport for HTML img" cellspacing="0" cellpadding="0"><tr><td align="center"><img src="pics/packages.png"></td></tr></table></div><p>
The public API of CORAL is defined in the <span class="bold"><b>CoralBase</b></span>
and <span class="bold"><b>RelationalAccess</b></span> packages. The former contains the definitions
of the name/type/value structures (<span class="emphasis"><em>AttributeList</em></span> and related classes) which are used
as row buffers for writing and reading data from a database and a <span class="emphasis"><em>Blob</em></span> C++ type
which is used for I/O with table columns of SQL type BLOB. The latter contains the user- and developer-level
interfaces (abstract classes) which expose the full functionality of CORAL. A client software component
is expected to include the header files only from these two packages and link against the two corresponding
libraries.
</p><p>The interfaces defined in RelationalAccess are implemented in several component libraries which are
loaded at run time and only at demand by the SEAL plugin management mechanism. Shared implementation code is
kept in the <span class="emphasis"><em>CoralCommon</em></span> package. With this arrangement we have achieved to minimize the
number of dependency levels and code duplication across the various packages.</p><p>Currently there are
</p><div class="itemizedlist"><ul type="disc"><li>four RDBMS-specific implementations for the interface set related to the functionality
of accessing data in a relational database (Oracle, SQLite, MySQL and Frontier),</li><li>two implementations of the interface set related to the retrieval of
authentication credentials (one based on XML files and another on environment variables),</li><li>an implementation of the interface set responsible for peforming the necessary
technology dispatching given a connection string (RelationalService),</li><li>two implementations of the interface set responsible for peforming logical to
physical database service lookup operations (one based on XML files and another on LFC),</li><li>an implementation based on LFC for both the lookup and authentication service funtionality,
</li><li>a simple implementation of the interface set responsible for registering and reporting
monitoring events generated by the RDBMS plugins (MonitoringService),</li><li>an implementation of the interface set responsible for managing open connections on the
client and for the overall system configuration (ConnectionService)</li></ul></div><p>
</p><p>In addition to the above, there is also the PyCoral package,
which provides a <span class="bold"><b>coral</b></span> python module which exposes the CORAL User API in python.
</p><p>
The CORAL architecture allows the development of additional or alternative implementations of the base
interfaces which can work immediatelly with the rest of the system. The only necessary requirement in order
to achieve that is the strict compliance to the interface semantics.
</p></div></div><div class="navfooter"><hr><table width="100%" summary="Navigation footer"><tr><td width="40%" align="left"><a accesskey="p" href="Scope.html">Prev</a>&nbsp;</td><td width="20%" align="center"><a accesskey="u" href="index.html">Up</a></td><td width="40%" align="right">&nbsp;<a accesskey="n" href="DevelopingClientComponents.html">Next</a></td></tr><tr><td width="40%" align="left" valign="top">2. Software Project Scope&nbsp;</td><td width="20%" align="center"><a accesskey="h" href="index.html">Home</a></td><td width="40%" align="right" valign="top">&nbsp;4. Developing Client Software Components</td></tr></table></div></body></html>