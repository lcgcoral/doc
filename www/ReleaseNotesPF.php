<?php

//-----------------------------------------------------------------------------
// Include another page from the local webserver
// See http://drupal.org/node/40756 (replace 'return' by 'echo' outside Drupal)
// See http://stackoverflow.com/questions/5588639
// [NB fails for external URLs! See http://stackoverflow.com/questions/9196228]
//-----------------------------------------------------------------------------

//ob_start();
//include_once dirname(__FILE__) . '/ReleaseNotesPF.txt';
//$out = ob_get_contents();
//ob_end_clean();

//-----------------------------------------------------------------------------
// Include another page from an external URL
// See http://stackoverflow.com/questions/9196228
// See http://php.net/manual/en/function.file-get-contents.php
//-----------------------------------------------------------------------------

$out = file_get_contents('http://svn.cern.ch/guest/lcgcoral/doc/www/ReleaseNotesPF.txt');

//-----------------------------------------------------------------------------
// Format the output using regular expressions
// See http://php.net/manual/en/function.preg-replace.php
// See http://www.catswhocode.com/blog/15-php-regular-expressions-for-web-developers
// See http://en.wikipedia.org/wiki/Regular_expression
//-----------------------------------------------------------------------------

// Remove comments (lines beginning with '#')
//$out = preg_replace('/(\n)#([^\n]+)(\n)/','${1}',$out);
$out = preg_replace('/(\n)(#([^\n]+)(\n))+/','${1}',$out);

//-----------------------------------------------------------------------------

// Cleanup. Remove all trailing spaces
$out = preg_replace('/([ ]+)(\n)/','$2',$out);
//echo $out; exit(0);

//-----------------------------------------------------------------------------

// Remove comments (lines beginning with '%%%')
$out = preg_replace('/(\n)%%%STARTDETAILS/','${1}%%STARTDETAILS',$out);
$out = preg_replace('/(\n)%%%ENDDETAILS/','${1}%%ENDDETAILS',$out);
$out = preg_replace('/(\n)%%%([^\n]+)(\n)/','${1}',$out);
$out = preg_replace('/(\n)%%STARTDETAILS/','${1}%%%STARTDETAILS',$out);
$out = preg_replace('/(\n)%%ENDDETAILS/','${1}%%%ENDDETAILS',$out);

//-----------------------------------------------------------------------------

// Header. Add an anchor for 'LCGCMT xxx ()' between two '===..\n'
$out = preg_replace('/==(=+)(\n)LCGCMT ([0-9])([0-9])([a-z]*|-patches|rc[0-9]|_root6|root6) ([^=]+)(\n)==(=+)/','<a name="LCGCMT_${3}${4}${5}"></a>==${1}${2}LCGCMT ${3}${4}${5} ${6}${7}==${8}',$out);

// Header. Add an anchor for 'LCG xxx ()' between two '===..\n'
$out = preg_replace('/==(=+)(\n)LCG ([0-9])([0-9])([a-z]*|-patches|rc[0-9]|_root6|root6|swan[0-9]) ([^=]+)(\n)==(=+)/','<a name="LCG_${3}${4}${5}"></a>==${1}${2}LCG ${3}${4}${5} ${6}${7}==${8}',$out);

// Header. Add an anchor for 'CORAL xxx ()' between two '===..\n'
$out = preg_replace('/==(=+)(\n)CORAL ([0-9]+).([0-9]+).([0-9]+)([a-z]*) ([^=]+)(\n)==(=+)/','<a name="CORAL_${3}_${4}_${5}${6}"></a>==${1}${2}CORAL ${3}.${4}.${5}${6} ${7}${8}==${9}',$out);

// Toggle detailed view (default: detailed view is off).
// The detailed view (if present) is show in italics.
// Add an anchor to reload the page at the same spot when toggling.
// Use the lazy operator '?' to match the least text before the first 'CORAL'.
// [TO DO: this may still break if the detailed text contains a '=' character?]
function replace_details($matches)
{
  //return "THEDEBUG";
  // See http://php.net/manual/en/function.preg-replace-callback.php
  // $matches[0] is the complete match
  // $matches[1] the match for the 1st subpattern enclosed in '(...)' and so on
  $out = $matches[4];
  //$out = preg_replace('/\{([^\)]+)\}([^=]+)(\n)==(=+)(\n)([^=]+?)(\n)(CORAL)([^=]+)(\n)([^!]*?)/','THESTART 1:${1} 2:${2} 3:${3} 4:${4} 5:${5} 6:${6} 7:${7} 8:${8} 9:${9} 10:${10} 11:${11} THEEND',$out);
  if ( isset( $_GET["detailed"] ) && $_GET["detailed"] == "yes" ) 
  {
    $out = preg_replace('/\{([^\)]+)\}([^=]+)(\n)==(=+)(\n)([^=]+?)(\n)(CORAL)([^=]+)(\n)([^!]*?)/','${3}<a name="${1}"></a>${3}==${4}${5}${2}${3}==${4}${5}${6}- <em><a href="?detailed=no#${1}">Hide details for pre-release tags.</a></em>${7}<em>${7}${7}${8}${9}</em>${7}${10}',$out);
  }
  else
  {
    $out = preg_replace('/\{([^\)]+)\}([^=]+)(\n)==(=+)(\n)([^=]+?)(\n)(CORAL)([^=]+)(\n)([^!]*?)/','${3}<a name="${1}"></a>${3}==${4}${5}${2}${3}==${4}${5}${6}- <em><a href="?detailed=yes#${1}">Show details for pre-release tags.</a></em>${7}${10}',$out);
  }
  return $out;
}
$out = preg_replace_callback('/(\n)==(=+)(\n)%%%STARTDETAILS([^@]*?)%%%ENDDETAILS/','replace_details',$out); // OK for 2.3.23 AND 2.3.16
//$out = preg_replace_callback('/(\n)==(=+)(\n)%%%STARTDETAILS([^!]*?)%%%ENDDETAILS/','replace_details',$out); // OK for 2.3.23, NOT OK for 2.3.16...
//echo $out; exit(0);

// Header. Format lines between two '===..\n' as <hr /><h2>..</h2>
$out = preg_replace('/==(=+)\n([^=]+)\n==(=+)/','<hr /><font color=red><h2>${2}</h2></font>',$out);

//-----------------------------------------------------------------------------

// Single horizontal line. Format a single '===..\n' as <hr />
$out = preg_replace('/==(=+)/','<hr />',$out);

//-----------------------------------------------------------------------------

// Bullet lists. The algorithm is inside outwards (start with nested bullets):
// - wrap inner bullet of level N on a single line 
//   (remove leading continuation spaces)
// - format all bullets of level N on a single <li>..</li> line
// - wrap N+1 bullets </li>\n<ul> on N bullet line </li><ul>
// - wrap N bullets with children </ul>\n<li> on a single line </ul><li>
// - wrap N bullets without children </li>\n<li> on a single line </li><li>
// - format '\n<li>..</li>\n' as '\n<ul><li>..</li><\ul>\n'
//   [assuming that leading <li> are only level N bullets]
// - iterate to level N-1 bullet

function wrapChildren($in)
{
  $out = $in;
  // Remove (one or more) \n in '</li>\n<ul>', '</ul>\n<li>', '</li>\n<li>'.
  $out = preg_replace('/<\/li>([\n]+)<ul>/','</li><ul>',$out);
  $out = preg_replace('/<\/ul>([\n]+)<li>/','</ul><li>',$out);
  $out = preg_replace('/<\/li>([\n]+)<li>/','</li><li>',$out);
  // Format '\n<li>..\n' as '\n<ul><li>..<\ul>\n'.
  // Format '^<li>..\n' as '^<ul><li>..<\ul>\n'.
  $out = preg_replace('/(\n|^)<li>([^\n]+)(\n)/','$1<ul><li>$2</ul>$3',$out);
  // Return the formatted output.
  return $out;
}

// Wrap non-empty lines with leading character other than '<', '-', '>'
// (optionally preceded by spaces), only if they follow a non-empty line.
while ( $out != $old )
{ 
  $old = $out;
  $out = preg_replace('/([^\n])\n( *[^<\-\+>\n ])([^(\n)]*)/','$1 $2$3',$old);
}

// 3. Format lines with leading '+' (after any space) as <li>..</li>.
$out = preg_replace('/(\n) *\+([^\n]+)/','$1<li>$2</li>',$out);
// Wrap the children.
$out = wrapChildren( $out );

// 2. Format lines with leading '>' (after any space) as <li>..</li>.
$out = preg_replace('/(\n) *>([^\n]+)/','$1<li>$2</li>',$out);
// Wrap the children.
$out = wrapChildren( $out );

// 1. Format lines with leading '-' (after any space) as <li>..</li>.
$out = preg_replace('/(\n) *\-([^\n]+)/','$1<li>$2</li>',$out);
// Wrap the children.
$out = wrapChildren( $out );

// 0. Format non-empty lines (including the first line in the file)
// with leading non-'<' (after any space) as <li>..</li>. 
$out = preg_replace('/(\n|^)( *[^(<|\n))])([^(\n)]+)/','$1<li><strong>$2$3</strong></li>',$out);
// Wrap the children.
$out = wrapChildren( $out );

//-----------------------------------------------------------------------------

// Format http links 'release notes (http://..)'
$out = preg_replace('/release notes \((https?:\/\/[^)]+)\)/','<a href="${1}">release notes</a>',$out);

// Format http links '(http://..)'
$out = preg_replace('/\((https?:\/\/[^)]+)\)/','(<a href="${1}">${1}</a>)',$out);

//-----------------------------------------------------------------------------

// Format Savannah links
$out = preg_replace('/bug #(\d+)/i','<a href="http://savannah.cern.ch/bugs/?${1}">bug #${1}</a>',$out);
$out = preg_replace('/task #(\d+)/i','<a href="http://savannah.cern.ch/task/?${1}">task #${1}</a>',$out);
$out = preg_replace('/sr #(\d+)/i','<a href="http://savannah.cern.ch/support/?${1}">sr #${1}</a>',$out);

// Format Jira links
$out = preg_replace('/CORALCOOL-(\d+)/i','<a href="https://sft.its.cern.ch/jira/browse/CORALCOOL-${1}">CORALCOOL-${1}</a>',$out);
$out = preg_replace('/CC-(\d+)/i','<a href="https://sft.its.cern.ch/jira/browse/CORALCOOL-${1}">CORALCOOL-${1}</a>',$out);
$out = preg_replace('/PF-(\d+)/i','<a href="https://sft.its.cern.ch/jira/browse/PF-${1}">PF-${1}</a>',$out);
$out = preg_replace('/ROOT-(\d+)/i','<a href="https://sft.its.cern.ch/jira/browse/ROOT-${1}">ROOT-${1}</a>',$out);
$out = preg_replace('/SPI-(\d+)/i','<a href="https://sft.its.cern.ch/jira/browse/SPI-${1}">SPI-${1}</a>',$out);
$out = preg_replace('/FTAPPDEVEL-(\d+)/i','<a href="https://its.cern.ch/jira/browse/FTAPPDEVEL-${1}">FTAPPDEVEL-${1}</a>',$out);

// Format SNOW links
$out = preg_replace('/INC(\d\d\d\d\d\d)/i','<a href="https://cern.service-now.com/service-portal/view-incident.do?n=INC${1}">INC${1}</a>',$out);

// Format GGUS links
$out = preg_replace('/GGUS-(\d\d\d\d\d\d)/i','<a href="https://ggus.eu/ws/ticket_info.php?ticket=${1}">GGUS-${1}</a>',$out);

// Format link to GIT diff and lcgsoft diff for cmake-based lcg configuration
// http://stackoverflow.com/questions/6613000/if-statement-inside-preg-replace
$out = preg_replace_callback('/<li> LCG_((\d+)([a-z]*|rc[0-9]|swan[0-9]))(( \(was LCG_((\d*)([a-z]*|rc[0-9]|swan[0-9]))\))?)<\/li>((<ul>)?)<li>/i', create_function('$matches','$txt="<li> LCG_".$matches[1]."</li><ul><li>See the corresponding <a href=https://gitlab.cern.ch/sft/lcgcmake/blob/LCG_".$matches[1]."/cmake/toolchain/heptools-".$matches[1].".cmake>heptools-".$matches[1].".cmake</a> on gitlab.cern.ch"; if ($matches[6]!="") $txt=$txt." and the <a href=http://pool.cern.ch/coral/LcgHeptoolsDiff.php?new=".$matches[1]."&old=".$matches[6].">changes</a> with respect to heptools-".$matches[6].".cmake"; $txt=$txt.".</li><li>See the corresponding <a href=http://lcgsoft.web.cern.ch/lcgsoft/release/".$matches[1].">LCG_".$matches[1]."</a> entry in lcgsoft.cern.ch"; if ($matches[6]!="") $txt=$txt." and the <a href=http://lcgsoft.web.cern.ch/lcgsoft/compare/".$matches[1]."/".$matches[6].">changes</a> with respect to LCG_".$matches[6]; $txt=$txt.".</li>".$matches[9]."</ul><li>"; return $txt;'), $out);

// Format link to SVN diff for non-cmake-based lcgcmt configuration (using trac)
// http://stackoverflow.com/questions/6613000/if-statement-inside-preg-replace
$out = preg_replace_callback('/<li> LCGCMT((_|-)(\d+)([a-z]*|rc[0-9]|_root6|root6))(( \(was (LCGCMT((_|-)\d*)([a-z]*|rc[0-9]|_root6|root6))\))?)<\/li>((<ul>)?)<li>/i', create_function('$matches','$txt="<li> LCGCMT".$matches[1]."</li><ul><li>See the corresponding <a href=https://svnweb.cern.ch/trac/lcgsoft/browser/tags/LCGCMT".$matches[1]."/lcgcmt/LCG_Configuration/cmt/requirements>LCG_Configuration</a> on svnweb.cern.ch."; if ($matches[7]!="") $txt=$txt."</li><li> See the changes in SVNDIFF(LCG_Configuration,tags/".$matches[7].",tags/LCGCMT".$matches[1].") since ".$matches[7]."."; $txt=$txt."</li>".$matches[11]."</ul><li>"; return $txt;'), $out);

// Format link to SVN diff for lcgcmt (using trac)
// [NB: internal ()'s are counted too!]
// Example: SVNDIFF(LCG_Interfaces/Reflex,tags/LCGCMT_64a,tags/LCGCMT_64b)
$out = preg_replace('/SVNDIFF\((LCG_[a-z]*(\/[a-z]*)?),((tags|branches)\/LCGCMT(_|-)(\d*)([a-z]*|rc[0-9]|_root6|root6)),((tags|branches)\/LCGCMT(_|-)(\d*|root6)([a-z]*|rc[0-9]-patches|_root6|root6|))\)/i','<a href="https://svnweb.cern.ch/trac/lcgsoft/changeset?old_path=/${3}/lcgcmt/${1}/cmt/requirements&new_path=/${8}/lcgcmt/${1}/cmt/requirements">${1}</a>',$out);

// Format link to SVN changeset (using trac)
// Example: SVNCHANGESET(lcgsoft,5763)
$out = preg_replace('/SVNCHANGESET\((lcgcool|lcgcoral|lcgsoft),(\d+)\)/i','<a href="https://svnweb.cern.ch/trac/${1}/changeset/${2}">${1}:${2}</a>',$out);

//-----------------------------------------------------------------------------

// Show table of contents (parse out all anchors in the formatted output)
preg_match_all('|a name="([^"]+?)"></a|',$out,$anchors);
//echo "<h6>Jump to a release:</h6><ul>";
//foreach( $anchors[1] as $anchor )
//  echo "<li><a href=\"#".$anchor."\">".$anchor."</a></li>";
//echo "</ul>";
// EVENTUALLY tune this to always show LCGCMT_67a/b last..
// [simply quit loop when $anchor is LCCMT_67a?]
echo "Jump to one of the 10 most recent releases:<ul>";
foreach( array_slice( $anchors[1], 0, 10 ) as $anchor )
  echo "<li><a href=\"#".$anchor."\">".$anchor."</a></li>";
echo "</ul>\n";

//-----------------------------------------------------------------------------

// Show the formatted output
echo $out;

//-----------------------------------------------------------------------------

?>
